import { RouteObject } from 'react-router-dom'
import { Demo } from 'demo'
import { AuthLayout, MainLayout } from 'components/layouts'
import { Account, Home, Login, Register } from 'pages'
import { PATH } from 'constant'
import Purchase from 'pages/Purchase'
import Detail from 'pages/Detail'

export const router: RouteObject[] = [
    {
        path: '/demo',
        element: <Demo />,
    },
    {
        path: '/',
        element: <MainLayout />,
        children: [
            {
                index: true,
                element: <Home />,
            },
            {
                path: PATH.account
                ,element: <Account/>
            },
            {
                path: PATH.purchase
                ,element: <Purchase/>
            }
        ],
    },{
        path: PATH.moviedetail,
        element: <Detail />,
    },
    {
        element: <AuthLayout />,
        children: [
            {
                path: PATH.login,
                element: <Login />,
            },
            {
                path: PATH.register,
                element: <Register />,
            },
          
        ],
    },
]
