
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState, useAppDispatch } from "store";
import { getCumRapThunk, getLichChieuThunk } from "store/quanLyRap/thunk";
// import { Tabs } from "components/ui";
import ThongTinlichChieuTab from "./ThongTinlichChieuTab";
import { quanLyNguoiDungActions } from "store/quanLyNguoiDung/slice";


export const CumRapTab = () => {
  // console.log(maHeThongRap);
  
  const dispatch = useAppDispatch();
  const { cumRap } = useSelector(
    (state: RootState) => state.quanLyRap
    );
    const {chooseCinema,chooseCumRap  } = useSelector( (state: RootState) => state.quanLyNguoiDung
    );
console.log("chooo",chooseCumRap)
    const handleTabClick = (key: any) => {
      dispatch(quanLyNguoiDungActions.changeCumRap(key));
  };
  useEffect(() => {
    dispatch(getCumRapThunk(chooseCinema));
    dispatch(getLichChieuThunk(chooseCinema))
   
   
  }, [chooseCinema]);
  
  return (
    // <div className="h-[500px]">
    //   <Tabs
    //     tabPosition="left"
    //     destroyInactiveTabPane={true}
    //     className="h-full"
    //     onTabClick={handleTabClick}
    //     tabBarGutter={-5}
    //     items={
    //       cumRap &&
    //       cumRap.map((rap,i) => {
    //         const id = String(i + 1);
    //         return  {
    //           label: (
    //             <div className="w-72 text-start flex flex-col px-1" key={rap.maCumRap}>
    //               <p className="text-xl font-semibold">{rap.tenCumRap}</p>
    //               <p className="break-all ">{rap.diaChi}</p>
    //             </div>
    //           ),
    //           key: id,
    //           children: <ThongTinlichChieuTab />,
    //         };
    //       })
    //     }
    //   />
      
    // </div>
    <div>{
      cumRap.map((rap,i) => {
             
                return  <div onClick={()=>{handleTabClick}}  className="w-72 text-start flex flex-col px-1" key={rap.maCumRap}>
                <p className="text-xl font-semibold">{rap.tenCumRap}</p>
                <p className="break-all ">{rap.diaChi}</p>
              </div>
                
              })}
      
   <ThongTinlichChieuTab/>
      </div>

  );
};

export default CumRapTab;
