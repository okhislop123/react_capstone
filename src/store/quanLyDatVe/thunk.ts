import { createAsyncThunk } from "@reduxjs/toolkit";
import { quanLyPhongVeService } from "services/quanLyDatVe";

export const getDanhSachPhongVeThunk = createAsyncThunk(
  "QuanLyDatVe/LayDanhSachPhongVe",
  async (payload: string | undefined, { rejectWithValue }) => {
    try {
      const query = `?MaLichChieu=${payload}`;
      const data = await quanLyPhongVeService.getDanhSachPhongVe(query);
      return data.data.content;
    } catch (error) {
      rejectWithValue(error);
    }
  }
);
