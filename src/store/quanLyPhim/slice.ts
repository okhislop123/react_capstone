import { createSlice } from '@reduxjs/toolkit'
import {  Banner, Movie } from 'types'
import { getBannerThunk, getMovieDetailThunk, getMovieListThunk } from './thunk'

type QuanLyPhimInitialState = {
    movieList: Movie[]
    banner:Banner[]
    currentPage: number
    isFetchingMovieList: boolean
    movie:  Movie | undefined;
}

const initialState: QuanLyPhimInitialState = {
    movieList: [],
    banner:[],
    isFetchingMovieList: false,
    currentPage: 1,
    movie:undefined

}

const quanLyPhimSlice = createSlice({
    name: 'quanLyPhim',
    initialState,
    reducers: {
        setCurrentPage: (state, action) => {
            state.currentPage = action.payload;
          },
    },
    extraReducers: (builder) => {
        builder
            .addCase(getMovieListThunk.pending, (state) => {
                state.isFetchingMovieList = true
            })
            .addCase(getMovieListThunk.fulfilled, (state, { payload }) => {
                state.movieList = payload
                state.isFetchingMovieList = false
            })
            .addCase(getMovieListThunk.rejected, (state) => {
                state.isFetchingMovieList = false
            })
            .addCase(getBannerThunk.fulfilled,(state,{payload})=>{
                state.banner=payload
            })
            .addCase(getMovieDetailThunk.fulfilled, (state, { payload }) => {
                state.movie = payload;
              })
    },
})

export const { reducer: quanLyPhimReducer, actions: quanLyPhimActions } = quanLyPhimSlice
